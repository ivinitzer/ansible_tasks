#!/bin/bash
#logfile = '/var/log/rm_db_files.log'
echo "Start at" `date +20\%y-\%m-\%d_\%H:\%M` >> "/var/log/inspect.log"
cd "/opt/chef-repo"
pwd >> "/var/log/inspect.log"
/opt/chefdk/embedded/bin/ruby cookbooks/inspec/recipes/main.rb >> "/var/log/inspect.log"
echo "END in" `date +20\%y-\%m-\%d_\%H:\%M` >> "/var/log/inspect.log"
