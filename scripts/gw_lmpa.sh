#!/bin/bash
#logfile = '/var/log/rm_db_files.log'
echo "Start at" `date +20\%y-\%m-\%d_\%H:\%M` >> "/var/log/gw_lmpa.log"
cd "/opt/chef-repo"
pwd >> "/var/log/gw_lmpa.log"
/opt/chefdk/embedded/bin/ruby cookbooks/gw_lmpa/recipes/gw_lmpa.rb >> "/var/log/gw_lmpa.log"
echo "END in" `date +20\%y-\%m-\%d_\%H:\%M` >> "/var/log/gw_lmpa.log"
