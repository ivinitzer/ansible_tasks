#!/bin/bash
log_file='/var/log/backup_lmpa_gw.log'
echo "Start at" `date +20\%y-\%m-\%d_\%H:\%M` > $log_file
cd "/opt/chef-repo"
pwd >> $log_file
/opt/chefdk/embedded/bin/chef-apply cookbooks/backup_lmpa_gw/recipes/bootstrap_cron.rb &> $log_file
echo "END in" `date +20\%y-\%m-\%d_\%H:\%M` >> $log_file
