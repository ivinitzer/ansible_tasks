#!/bin/bash
#logfile = '/var/log/rm_db_files.log'
echo "Start at" `date +20\%y-\%m-\%d_\%H:\%M` >> "/var/log/gwmove.log"
cd "/opt/chef-repo"
pwd >> "/var/log/gwmove.log"
/opt/chefdk/embedded/bin/ruby cookbooks/gwmove/recipes/main.rb 
echo "END in" `date +20\%y-\%m-\%d_\%H:\%M` >> "/var/log/gwmove.log"
