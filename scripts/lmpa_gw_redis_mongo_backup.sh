#!/bin/bash
# Author: yoram
# ------------------------------------------
read_all_lmpa_gw_envs_into_inventory_string() {
	
	INPUT="/samba/chef/Environment_Information_Abacus.csv"
	OLDIFS=$IFS
	IFS=','
	[ ! -f $INPUT ] && { echo "$INPUT file not found"; exit 99; }

	old_server="localhost"
	NL=$'\n'
	foo="[tt]${NL}"

	while read env_name server_name server_ip site code status garbage
	do

		
		((line_number++))
		if [ $line_number -eq 0 ]; then
			continue
		fi
		server_name=$(echo "${server_name}" | awk '{print toupper($0)}') 
		server_ip=$(echo "${server_ip}" | awk '{print toupper($0)}')
		status=$(echo "${status}" | awk '{print toupper($0)}')
		if [ -z $server_ip ] ; then
			server_ip=$(ping -c 1 $server_name | gawk -F'[()]' '/PING/{print $2}')
		fi
		if [[ $server_name =~ "LMPA" ]]; then
			workingpass=$prodrootpass_lmpa
		elif [[ $server_name =~ "GW" ]]; then
			workingpass=$prodrootpass_gw
		fi
		
		if [[ $server_name =~ "LMPA" ]]||[[ $server_name =~ "GW" ]]; then
			  #copy sshkey to wl server

				ping -c1 -W1 -q $server_name &>/dev/null
				status=$( echo $? )
				if [[ $status == 0 ]] ; then
					#echo "Connection $server_name success!"
					#sshpass -p "$workingpass" ssh-copy-id $server_ip&>/dev/null
					foo="${foo}${server_ip} hostname=$server_name ansible_ssh_user=root ansible_ssh_pass=${workingpass}${NL}"
				fi

		fi

	done < $INPUT
	echo $foo>/tmp/test.txt
	func_result=$foo
}

prodrootpass_gw="@B@cu5GW2017"
prodrootpass_lmpa="@B@cu5LMP@2017"

ansible_root_folder="/opt/tt-ansible/redis_mongo_backup"
#ansible files location

backup_DIR_redis="/opt/tt-ansible/tgz_temp_redis"
backup_DIR_mongo="/opt/tt-ansible/tgz_temp_mongo"
remote_path_redis="/tmp/tgz_temp_redis"
remote_path_mongo="/tmp/tgz_temp_mongo"

f_inventory="${ansible_root_folder}/inventory_d"
f_main="${ansible_root_folder}/roles/lmpa_gw_redis_mongo_backup/tasks/main.yml"
line_number=-1

sed -i -e "s#<REMOTE_PATH_REDIS>#$remote_path_redis#g" $f_main
sed -i -e "s#<REMOTE_PATH_MONGO>#$remote_path_mongo#g" $f_main
sed -i -e "s#<CHEF_PATH_REDIS>#$backup_DIR_redis#g" $f_main
sed -i -e "s#<CHEF_PATH_MONGO>#$backup_DIR_mongo#g" $f_main

mkdir -p $backup_DIR_redis
mkdir -p $backup_DIR_mongo

#build list of servers for lmpa/gw inventory
read_all_lmpa_gw_envs_into_inventory_string
foo=$func_result

echo $foo>/tmp/test.txt
echo $foo | tee $f_inventory

cd  $ansible_root_folder		
sudo ansible-playbook -i inventory_d site.yml --tags "lmpa_gw_redis_mongo_backup"  -vvv

cd /opt/scripts
sudo python3 upload_redis_mongo_to_s3.py

IFS=$OLDIFS
