#!/bin/bash
#/opt/scripts/run_installtion
#sed -i 's/\r//g' run_installtion.sh
#description:run whitlabel new installion and prepare the ansible enverument folders
#steps:
#1.read csv configuraion file
#2.remove old folder and download new source from bitbucket 
#3.replace configuration file.
#4.execute the ansible-playbook process

#exit if bash is running
echo "ssssssssssssssssssssssss"
ps aux | grep wl_installtion.sh | grep -q R+
if [ $? -ne 1 ];then
 exit 1
fi

#color for the sceen
red=$(tput setaf 1)
#white = $(tput setaf 7)
green=$(tput setaf 2)
yellow=$(tput setaf 3)
byellow=$(tput setaf 43)
cyan=$(tput setaf 6)
reset=$(tput sgr0)

input="/samba/chef/white_label.csv"
ansible_root_folder="/opt/tt-ansible"

prodrootpass="@B@cu5GW2017"
devrootpass="triltest"
workingpass=$prodrootpass
hostname=`hostname`
if [ $hostname = 'chefwork.pfslab.local' ];then
  workingpass=$devrootpass
fi

#const bitbucket location
agileCode_deployment="https://israelv:OtherPass@bitbucket.ideasoft.io/scm/tt/deployment.git"
tt_deployment="https://software%40tradertools.com:proIT20!!@bitbucket.org/whitelabel_team/deployment.git"
gitdeployment=$agileCode_deployment

#bash log location
f_whitlabel_log="/var/log/whitelabel/whitlabel.log"
#ansible files location
f_inventory="/opt/tt-ansible/inventory"
f_enviroment="/opt/tt-ansible/roles/tt/templates/enviroment.json"
f_config="/opt/tt-ansible/roles/tt/templates/config.ts"
f_main="/opt/tt-ansible/roles/tt/tasks/main.yml"
f_deploy="/opt/tt-ansible/roles/tt/files/deploy.sh" 
f_market="/opt/tt-ansible/roles/tt/files/market.properties" 
f_trading="/opt/tt-ansible/roles/tt/files/trading.properties" 

#remove windows newline
sed -i 's/\r//g' $input

#number of csv lines
line_number=-1
function log()
 {
   echo $(date +%F-%H:%M:%S) $1 
   echo $(date +%F-%H:%M:%S) $1 >> $f_whitlabel_log 
 } 
echo "${yellow}create log at $f_whitlabel_log ${reset}"
#loop on csv file configuration
while IFS=',' read -r Run Server Version FixGateway  deployment; do
  ((line_number++))
  # execute only when run equal to 1
  log "${reset} INFO:start processing csv line number:$line_number "
  if [ $line_number -eq 0 ]; then
   continue
  fi
  
  if [ $Run != 1 ]; then
    log "${reset} INFO:server $Server not configure to run"
   continue
  fi
  log "${reset} INFO:checking server $Server input for whitelabel installion"

  #check csv mandatory parameters  $Server $Version $FixGateway
  if [ -z $Server ] || [ -z $Version ] || [ -z $FixGateway ]; then
    log "${yellow}ERROR: ${red}error in $input, line number $line_number .one or more mandatory columns (Server,Version,FixGateway)is empty${reset}"
    log "${green}INFO: moving to the next server installtion ${reset}"
    continue
  fi
  #get ip address of whitelabel and fixGateway
  log "${green}INFO: start processing csv line number ($line_number) : Server:$Server Version:$Version FixGateway:$FixGateway ${reset}"
  ip_wl=$(ping -c 1 $Server | gawk -F'[()]' '/PING/{print $2}')
  ip_gw=$(ping -c 1 $FixGateway | gawk -F'[()]' '/PING/{print $2}')

  if [ -z $ip_wl ] || [ -z $ip_gw ]; then
    log "${yellow}ERROR: ${red}cannot resolve ip address from hostname, please check the ping of Server:$Server and FixGateway:$FixGateway ${reset}"
    log "${green}INFO: moving to the next server installtion ${reset}"
    continue
  fi
  #copy sshkey to wl server
  sshpass -p "$workingpass" ssh-copy-id $Server
  
  #check from where to download the deployment

  if [[ $deployment == "TT"* ]]; then #if deployment not ending good (csv windows ending line char)
       gitdeployment=$tt_deployment
  fi
  
  #remove dest folder clone the branch and check if exsits  
  log "${green}INFO: clear $ansible_root_folder${cyan}"
  rm -rf $ansible_root_folder
  #echo "clone git $Version to /opt/tt-ansible folder"
  git clone -b $Version --single-branch $gitdeployment $ansible_root_folder
  echo "${reset}"
  
  #check if directory/version exsits 
  if [ ! -d $ansible_root_folder ]; then
    log "${yellow}ERROR: ${red}${byellow} folder $ansible_root_folder was not created .
    the clone from the bitbucket with version:$Version not found in the remote bitbucket or you dont have permission.
    please check the version/permission for for Version:$Version ${reset}"
    log "${green}INFO: moving to the next server installtion ${reset}"
    continue    
  fi
  echo "${reset}"
  log "${green}INFO : strat replaceing file "

  log "INFO : updating inventory files "
  echo "${reset} ${cyan}"
  sed -i -e "s/localhost/$ip_wl/g;s/ansible_connection=local/ansible_user=root/g" $f_inventory
  
  log "${green}INFO : updating templates files "
  log "INFO : updating templates files : $f_enviroment "

  echo "${reset} ${cyan}"
  sed -i -e 's/ "machine": ""/"machine": "'"$ip_wl"'"/g' $f_enviroment
  sed -i -e "s/{{ http_host }}/$ip_wl/g" $f_enviroment
  
  log "${green}INFO : updating templates files : $f_config "
  echo "${reset} ${cyan}"
  sed -i -e "s/{{ http_host }}/$ip_wl/g" $f_config
  #add missing row TIMESYNC_ENABLED
  if  ! grep -q "TIMESYNC_ENABLED" "$f_config"; then 
    echo "export const TIMESYNC_ENABLED = true" >> $f_config 
  fi

  log "${green}INFO : updating $f_main "
  echo "${reset} ${cyan}"
  
  #when TT in Used we need to remove agilecode and add id_rsa_tt to the main.yml file
  if [[ $deployment == "TT"* ]]; then #if deployment not ending good (csv windows ending line char)
    #sed -i '/src=id_rsa/d' $f_main #remove agileCode id_rsa line from main.yml
    #sed -i -e '/package/{i\- copy: src=id_rsa_tt dest=/home/dev/.ssh/id_rsa_tt owner=dev group=dev mode=0600' -e ':a;$q;n;ba;}' $f_main #add id_rsa_tt
    #sed -i -e 's/ssh:\/\/git@bitbucket.ideasoft.io:7999\/tt/git@bitbucket.org:whitelabel_team/g' $f_main #change repository
    
    # --- sed 's/*\/frontend.git*/repo: https:\/\/software%40tradertools.com:proIT20!!@bitbucket.org\/whitelabel_team\/frontend.git/g' $f_main
    # -- sed 's/*backend-for-frontend.git*/repo: https:\/\/software%40tradertools.com:proIT20!!@bitbucket.org\/whitelabel_team\/backend-for-frontend.git/g' $f_main
    
    sed -i -e 's/ssh:\/\/git@bitbucket.ideasoft.io:7999\/tt\/backend-for-frontend.git/https:\/\/software%40tradertools.com:proIT20!!@bitbucket.org\/whitelabel_team\/backend-for-frontend.git/g' $f_main

    sed -i -e 's/ssh:\/\/git@bitbucket.ideasoft.io:7999\/tt\/frontend.git/https:\/\/software%40tradertools.com:proIT20!!@bitbucket.org\/whitelabel_team\/frontend.git/g' $f_main 
    
    #sed -i -e 's/key_file: \/home\/dev\/.ssh\/id_rsa/key_file: \/home\/dev\/.ssh\/id_rsa_tt/g' $f_main #chane the key_file to support id_rsa_tt
  fi
  log "${green}INFO : updating files folder "
  log "${green}INFO : updating files folder : $f_deploy"
  echo "${reset} ${cyan}"
  echo "${reset}" 
  sed -i -e 's/STAGE="dev"/STAGE="'"$Version"'"/g' $f_deploy
  log "${green}INFO : updating files folder : $deploy "
  
  
  log "${green}INFO : finish updating the server files. "
  log "${green}INFO : start running ansible process"
  cd  $ansible_root_folder
  echo "${reset}" 
  pwd
  sudo ansible-playbook -i inventory site.yml
   
done <"$input"
