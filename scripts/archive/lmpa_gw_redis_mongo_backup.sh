#!/bin/bash
# Author: yoram
# ------------------------------------------
read_all_lmpa_gw_envs_into_inventory_string() {
	
	INPUT="/samba/chef/Environment_Information_Abacus.csv"
	OLDIFS=$IFS
	IFS=','
	[ ! -f $INPUT ] && { echo "$INPUT file not found"; exit 99; }

	old_server="localhost"
	NL=$'\n'
	foo="[tt]${NL}"

	while read env_name server_name server_ip site code status garbage
	do

		
		((line_number++))
		if [ $line_number -eq 0 ]; then
			continue
		fi
		server_name=$(echo "${server_name}" | awk '{print toupper($0)}') 
		server_ip=$(echo "${server_ip}" | awk '{print toupper($0)}')
		status=$(echo "${status}" | awk '{print toupper($0)}')
		if [ -z $server_ip ] ; then
			server_ip=$(ping -c 1 $server_name | gawk -F'[()]' '/PING/{print $2}')
		fi
		if [[ $server_name =~ "LMPA" ]]; then
			workingpass=$prodrootpass_lmpa
		elif [[ $server_name =~ "GW" ]]; then
			workingpass=$prodrootpass_gw
		fi
		
		if [[ $server_name =~ "LMPA" ]]||[[ $server_name =~ "GW" ]]; then
			  #copy sshkey to wl server

				ping -c1 -W1 -q $server_name &>/dev/null
				status=$( echo $? )
				if [[ $status == 0 ]] ; then
					#echo "Connection $server_name success!"
					#sshpass -p "$workingpass" ssh-copy-id $server_ip&>/dev/null
					foo="${foo}${server_ip} hostname=$server_name ansible_ssh_user=root ansible_ssh_pass=${workingpass}${NL}"
				fi

		fi

	done < $INPUT
	echo $foo>/tmp/test.txt
	func_result=$foo
}

prodrootpass_gw="@B@cu5GW2017"
prodrootpass_lmpa="@B@cu5LMP@2017"

#git_url="https://software%40tradertools.com:proIT20!!@bitbucket.org/ivinitzer/ansible_lmpa_gw_backups.git"
#git_url="https://software%40tradertools.com:proIT20!!@bitbucket.org/customer_configuration/backup_customers_lmpa_gw.git"
git_url="git@bitbucket.org:customer_configuration/backup_customers_lmpa_gw.git"

ansible_root_folder="/opt/tt-ansible/redis_mongo_backup"
#ansible files location

git_path="/opt/tt-ansible/git_temp_redis_mongo"
backup_DIR_redis="/opt/tt-ansible/tgz_temp_redis_mongo"
backup_DIR_mongo="/opt/tt-ansible/tgz_temp_mongo"
remote_path_redis="/tmp/tgz_temp_redis"
remote_path_mongo="/tmp/tgz_temp_mongo"



f_inventory="${ansible_root_folder}/inventory_h"
f_main="${ansible_root_folder}/roles/lmpa_gw_redis_mongo_backup/tasks/main.yml"
line_number=-1

#remove dest folder clone the branch and check if exsits  
deploy_url="https://yoramv_releasemanage:Winrunner12345@bitbucket.org/ivinitzer/ansible_tasks.git"
# rm -rf $ansible_root_folder
# echo "clone git ${deploy_url} to ${ansible_root_folder} folder"
# git clone $deploy_url $ansible_root_folder





sed -i -e "s#<GIT_URL>#$git_url#g" $f_main
sed -i -e "s#<REMOTE_PATH_REDIS>#$remote_path_redis#g" $f_main
sed -i -e "s#<REMOTE_PATH_MONGO>#$remote_path_mongo#g" $f_main
sed -i -e "s#<CHEF_PATH_REDIS>#$backup_DIR_redis#g" $f_main
sed -i -e "s#<CHEF_PATH_MONGO>#$backup_DIR_mongo#g" $f_main



mkdir -p $backup_DIR_redis
mkdir -p $backup_DIR_mongo
mkdir -p $git_path

#build list of servers for lmpa/gw inventory
read_all_lmpa_gw_envs_into_inventory_string
foo=$func_result


echo $foo>/tmp/test.txt
echo $foo | tee $f_inventory


cd  $ansible_root_folder		
sudo ansible-playbook -i inventory_h site.yml --tags "lmpa_gw_redis_mongo_backup"  -vvv
#go to git path and clone
cd  $git_path
# rm -rf *
# git clone -b  master $git_url
#check if dir exist and copy
# if [ "$(ls -A $backup_DIR_redis)" ]; then
#	cd $git_path/backup_customers_lmpa_gw/Redis
#	cp $backup_DIR_redis/* .
# fi
# if [ "$(ls -A $backup_DIR_mongo)" ]; then

#	cd $git_path/backup_customers_lmpa_gw/Mongo
#	cp $backup_DIR_mongo/* .

# fi

# cd $git_path/backup_customers_lmpa_gw/
# git add --all
# git commit -am 'latest update'
# git push origin master


# rm -rf $backup_DIR_redis
# rm -rf $backup_DIR_mongo

IFS=$OLDIFS
