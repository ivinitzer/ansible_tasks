#!/bin/bash
# Author: yoram
# ------------------------------------------
read_all_lmpa_gw_envs_into_inventory_string() {
	
	INPUT="/samba/chef/Environment_Information_Abacus.csv"
	OLDIFS=$IFS
	IFS=','
	[ ! -f $INPUT ] && { echo "$INPUT file not found"; exit 99; }

	old_server="localhost"
	NL=$'\n'
	foo="[tt]${NL}"

	while read env_name server_name server_ip site code status garbage
	do

		
		((line_number++))
		if [ $line_number -eq 0 ]; then
			continue
		fi
		server_name=$(echo "${server_name}" | awk '{print toupper($0)}') 
		server_ip=$(echo "${server_ip}" | awk '{print toupper($0)}')
		status=$(echo "${status}" | awk '{print toupper($0)}')
		if [ -z $server_ip ] ; then
			server_ip=$(ping -c 1 $server_name | gawk -F'[()]' '/PING/{print $2}')
		fi
		if [[ $server_name =~ "LMPA" ]]; then
			workingpass=$prodrootpass_lmpa
		elif [[ $server_name =~ "GW" ]]; then
			workingpass=$prodrootpass_gw
		fi
		
		if [[ $server_name =~ "LMPA" ]]||[[ $server_name =~ "GW" ]]; then
			  #copy sshkey to wl server

				ping -c1 -W1 -q $server_name &>/dev/null
				status=$( echo $? )
				if [[ $status == 0 ]] ; then
					#echo "Connection $server_name success!"
					sshpass -p "$workingpass" ssh-copy-id $server_ip&>/dev/null
					foo="${foo}${server_ip} hostname=$server_name ansible_ssh_user=root ansible_ssh_pass=${workingpass}${NL}"
				fi

		fi

	done < $INPUT
	echo $foo>/tmp/test.txt
	func_result=$foo
}

prodrootpass_gw="@B@cu5GW2017"
prodrootpass_lmpa="@B@cu5LMP@2017"
devrootpass="triltest"

hostname=`hostname`
if [ $hostname = 'chefwork.pfslab.local' ];then
  prodrootpass_lmpa=$devrootpass
  prodrootpass_gw=$devrootpass
fi


git_url="git@bitbucket.org:customer_configuration/backup_customers_lmpa_gw.git"


ansible_root_folder="/opt/tt-ansible/conf_backup"
INPUT="/samba/chef/Environment_Information_Abacus.csv"
#ansible files location

backup_DIR_conf="/opt/tt-ansible/tgz_temp_conf"
remote_path_conf="/tmp/tgz_temp_conf"	
git_path="/opt/tt-ansible/git_temp_conf"

						 
f_inventory="${ansible_root_folder}/inventory_w"
f_main="${ansible_root_folder}/roles/lmpa_gw_conf_backup/tasks/main.yml"
line_number=-1

#remove dest folder clone the branch and check if exsits  
deploy_url="https://yoramv_releasemanage:Winrunner12345@bitbucket.org/ivinitzer/ansible_tasks.git"
rm -rf $ansible_root_folder
echo "clone git ${deploy_url} to ${ansible_root_folder} folder"
git clone $deploy_url $ansible_root_folder

sed -i -e "s#<GIT_URL>#$git_url#g" $f_main
sed -i -e "s#<REMOTE_PATH_CONF>#$remote_path_conf#g" $f_main
sed -i -e "s#<CHEF_PATH_CONF>#$backup_DIR_conf#g" $f_main						

mkdir -p $backup_DIR_conf										   
mkdir -p $git_path

#build list of servers for lmpa/gw inventory
read_all_lmpa_gw_envs_into_inventory_string
foo=$func_result
echo $foo>/tmp/test.txt
echo $foo | tee $f_inventory
cd  $ansible_root_folder		
	
sudo ansible-playbook -i inventory_w site.yml --tags "lmpa_gw_conf_backup"

if [ "$(ls -A $backup_DIR_conf)" ]; then
	cd  $git_path
	rm -rf *
	git clone -b  master $git_url
	cd $git_path/backup_customers_lmpa_gw/Config
	cp $backup_DIR_conf/* .
	git add --all
	git commit -am 'latest update'
	git push origin master
	rm -rf $backup_DIR_conf
fi
rm -rf $backup_DIR_conf
IFS=$OLDIFS
