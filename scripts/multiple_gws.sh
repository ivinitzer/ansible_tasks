#!/bin/bash
# Author: yoram
# ------------------------------------------
read_all_envs_into_inventory_string() {
	
	INPUT="/samba/chef/GW_Clients.csv"
	OLDIFS=$IFS
	IFS=','
	[ ! -f $INPUT ] && { echo "$INPUT file not found"; exit 99; }
	line_number=-1
	old_server="localhost"
	NL=$'\n'
	foo="[tt]${NL}"

	while read Execute Server NotRelevant
	do
		((line_number++))
		if [ $line_number -eq 0 ]; then
			continue
		fi		
		if [ "$Execute" == "0" ]; then	
			continue
		fi
		server_name=$(echo "${Server}" | awk '{print toupper($0)}') 
		server_ip=$(ping -c 1 $Server | gawk -F'[()]' '/PING/{print $2}')

		if [[ $server_name =~ "LMPA" ]]; then
			workingpass=$prodrootpass_lmpa
		elif [[ $server_name =~ "GW" ]]; then
			workingpass=$prodrootpass_gw
		fi
		
		ping -c1 -W1 -q $server_name &>/dev/null
		status=$( echo $? )
		if [[ $status == 0 ]] ; then
			#echo "Connection $server_name success!"
			sshpass -p "$workingpass" ssh-copy-id $server_ip&>/dev/null
			foo="${foo}${server_ip} hostname=$server_name ansible_user=root ansible_pass=${workingpass}${NL}"
		fi


	done < $INPUT
	echo $foo>/tmp/test.txt
	func_result=$foo
}

prodrootpass_gw="@B@cu5GW2017"
prodrootpass_lmpa="@B@cu5LMP@2017"
devrootpass="triltest"

hostname=`hostname`
if [ $hostname = 'chefwork.pfslab.local' ];then
  prodrootpass_lmpa=$devrootpass
  prodrootpass_gw=$devrootpass
fi


ansible_root_folder="/opt/tt-ansible/multiple_gws"
INPUT="/samba/chef/GW_Clients.csv"
						 
f_inventory="${ansible_root_folder}/inventory"

deploy_url="https://yoramv_releasemanager:Winrunner12345@bitbucket.org/whitelabel_team/deployment.git"
rm -rf $ansible_root_folder
mkdir $ansible_root_folder
cd  $ansible_root_folder
git clone -b "Multiple_GW" $deploy_url
cd deployment
#build list of servers for lmpa/gw inventory
read_all_envs_into_inventory_string
foo=$func_result
echo $foo>/tmp/test.txt
echo $foo | tee inventory
sudo ansible-playbook -i inventory site.yml

