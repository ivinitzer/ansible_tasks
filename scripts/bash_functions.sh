#!/bin/bash
# Author: yoram
# -----------
read_all_lmpa_gw_envs_into_inventory_string() {
	
	INPUT="/samba/chef/Environment_Information_Abacus.csv"
	OLDIFS=$IFS
	IFS=','
	[ ! -f $INPUT ] && { echo "$INPUT file not found"; exit 99; }

	old_server="localhost"
	NL=$'\n'
	foo="[tt]${NL}"

	while read env_name server_name server_ip site code status garbage
	do

		
		((line_number++))
		if [ $line_number -eq 0 ]; then
			continue
		fi
		server_name=$(echo "${server_name}" | awk '{print toupper($0)}') 
		server_ip=$(echo "${server_ip}" | awk '{print toupper($0)}')
		status=$(echo "${status}" | awk '{print toupper($0)}')
		if [ -z $server_ip ] ; then
			server_ip=$(ping -c 1 $server_name | gawk -F'[()]' '/PING/{print $2}')
		fi
		if [[ $server_name =~ "LMPA" ]]; then
			workingpass=$prodrootpass_lmpa
		elif [[ $server_name =~ "GW" ]]; then
			workingpass=$prodrootpass_gw
		fi
		
		if [[ $server_name =~ "LMPA" ]]||[[ $server_name =~ "GW" ]]; then
			  #copy sshkey to wl server

				ping -c1 -W1 -q $server_name &>/dev/null
				status=$( echo $? )
				if [[ $status == 0 ]] ; then
					#echo "Connection $server_name success!"
					sshpass -p "$workingpass" ssh-copy-id $server_ip&>/dev/null
					foo="${foo}${server_ip} hostname=$server_name${NL}"
				fi

		fi

	done < $INPUT
	echo $foo>/tmp/test.txt
	func_result=$foo
}