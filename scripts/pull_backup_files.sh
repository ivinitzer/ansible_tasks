#!/bin/bash
log_file='/var/log/pull_backup_files.log'
echo "Start at" `date +20\%y-\%m-\%d_\%H:\%M` > $log_file
cd "/opt/chef-repo"
pwd >> $log_file
/opt/chefdk/embedded/bin/chef-client -j cookbooks/backup_lmpa_gw/recipes/pull_backup_files.json  |& tee -a  $log_file
echo "END in" `date +20\%y-\%m-\%d_\%H:\%M` >> $log_file
