exists(){
  if [ "$2" != in ]; then
    echo "Incorrect usage."
    echo "Correct usage: exists {key} in {array}"
    return
  fi   
  eval '[ ${'$3'[$1]+muahaha} ]'  
}

declare -A arr_streams

file_path_source="/opt/tradertools/whitelabel/bff/fix"
file_path_target="/opt/tradertools/whitelabel/bff/fix"
tt_config_path="/opt/tradertools/whitelabel/bff/tt_config"
INPUT_CSV_FILE=GW_Clients.csv

foo=""
NL=$'\n'

list_of_gw=$(awk -F , '$2 == "<SERVER>" { print }' "${INPUT_CSV_FILE}")

for row in ${list_of_gw//\n\b/ } ; do
	#echo $row
	server=$(echo $row | cut -d',' -f2)
	FixName=$(echo $row | cut -d',' -f3)
	ClientNames=$(echo $row | cut -d',' -f4)

	IFS=';' read -ra arr_clients <<< "${ClientNames}"
	cnames=""
	for i in "${arr_clients[@]}"
	do
		cnames=$cnames\"${i}\",
	done
	#move last char 
	#echo ${cnames::-1}

	MD_SenderCompID=$(echo $row | cut -d',' -f5)
	MD_TargetCompID=$(echo $row | cut -d',' -f6)
	MD_SocketConnectionPort=$(echo $row | cut -d',' -f7)
	ORD_SenderCompID=$(echo $row | cut -d',' -f8)
	ORD_TargetCompID=$(echo $row | cut -d',' -f9)
	ORD_SocketConnectionPort=$(echo $row | cut -d',' -f10)
	
	if ! exists "\"${FixName}\"" in arr_streams; then 
		arr_streams[\"${FixName}\"]=""
	fi 
	arr_streams[\"${FixName}\"]="${arr_streams[\"${FixName}\"]}${cnames::-1}"

	
	md_file_name="${FixName}.market.properties"
	ord_file_name="${FixName}.trading.properties"
	if [[ ! -f ${file_path_target}/${md_file_name} ]]; then

		#copy ord and md files to fix directory and rename file with ClientName
		cp "${file_path_source}/market.properties" "${file_path_target}/${md_file_name}"
		cp "${file_path_source}/trading.properties" "${file_path_target}/${ord_file_name}"
		#modify MD
		sed -i -e "s/SenderCompID.*/SenderCompID=${MD_SenderCompID}/g" $file_path_target/$md_file_name
		sed -i -e "s/TargetCompID.*/TargetCompID=${MD_TargetCompID}/g"  $file_path_target/$md_file_name
		sed -i -e "s/SocketConnectPort.*/SocketConnectPort=${MD_SocketConnectionPort}/g"  $file_path_target/$md_file_name
		#modify ORD
		sed -i -e "s/SenderCompID.*/SenderCompID=${ORD_SenderCompID}/g"  $file_path_target/$ord_file_name
		sed -i -e "s/TargetCompID.*/TargetCompID=${ORD_TargetCompID}/g"  $file_path_target/$ord_file_name
		sed -i -e "s/SocketConnectPort.*/SocketConnectPort=${ORD_SocketConnectionPort}/g"  $file_path_target/$ord_file_name
	fi
done


if [[ ! -f ${tt_config_path}/account_to_stream.json ]]; then

	for key in ${!arr_streams[@]}; do
		#arr_streams[${key}]=${arr_streams[${key}]::-1}
		foo="${foo}${key}:[${arr_streams[${key}]}],"
		#echo ${arr_streams[${key}]::-1}
	done
	foo="{${foo::-1}}"
	echo $foo>"${tt_config_path}/account_to_stream.json"
fi
rm -f $INPUT_CSV_FILE